class ApplicationController < ActionController::Base
  include CrudHelper
  include Pagy::Backend

  before_action :configure_permitted_parameters, if: :devise_controller?

  helper_method :current_or_guest_user
  helper_method :referer_url

  def form_render
    build_form

    if params[:rerender]
      render_html_form
    end
  end

  def successful_create
    flash[:success] = create_success_msg

    respond_to do |format|
      format.js { redirect_to index_url }
      format.json { render_json_success }
      format.html { redirect_to index_url }
    end
  end

  def index_url
    url_for(action: :index)
  end

  def unsuccessful_create
    flash.now[:error] = create_error_msg

    respond_to do |format|
      format.js { render_html_form(error: true) }
      format.json { render_json_error }
      format.html { render referer_data[:action] }
    end
  end

  def successful_update
    flash.now[:success] = update_success_msg

    respond_to do |format|
      format.js { render_html_form }
      format.json { render_json_success }
      format.html { render referer_data[:action] }
    end
  end

  def unsuccessful_update
    flash.now[:error] = update_error_msg

    respond_to do |format|
      format.js { render_html_form(error: true) }
      format.json { render_json_error }
      format.html { render referer_data[:action] }
    end
  end

  def successful_destroy
    flash[:success] = destroy_success_msg

    redirect_to index_url
  end

  def unsuccessful_destroy
    flash.now[:error] = destroy_error_msg

    respond_to do |format|
      format.js { render_html_form(error: true) }
      format.json { render_json_error }
      format.html { render referer_data[:action] }
    end
  end

  # def load_user
  #  @user = current_or_guest_user
  # end

  def redirect_to(url, status: 302, normal: nil)
    # TODO: How can i properly pass in a status?
    return super(url, status: status) if normal

    respond_to do |format|
      format.js { tubolinks_redirect(url) }
      format.json { super(url, format: :json, status: status) }
      format.html { super(url, status: status) }
    end
  end

  def tubolinks_redirect(url)
    uri = URI.parse(url)
    path = uri.path
    path += "?#{uri.query}" if uri.query.present?

    render js: "Turbolinks.visit('#{path}')"
  end

  def render_html_form(model = @model,
                       status: nil,
                       error: false,
                       partial: "form")

    status ||= (:unprocessable_entity if error)

    build_form

    render partial: partial,
           layout: false,
           status: status,
           locals: { model: model }
  end

  def build_form
    nil
  end

  def render_json_error(model = @model)
    render(
      json: model.errors.messages.to_json,
      status: :unprocessable_entity,
    )
  end

  def render_json_success
    render json: { status: :success }
  end

  def referer_data
    Rails.application.routes.recognize_path(request.referer)
  end

  def referer_url
    request.referer.to_s
  end

  # protect_from_forgery

  # if user is logged in, return current_user, else return guest_user
  def current_or_guest_user
   if current_user
     if session[:guest_user_id] && session[:guest_user_id] != current_user.id
       logging_in
       # reload guest_user to prevent caching problems before destruction
       guest_user(with_retry = false).try(:reload).try(:destroy)
       session[:guest_user_id] = nil
     end
     current_user
   else
     guest_user
   end
  end

  ## find guest_user object associated with the current session,
  ## creating one as needed
  def guest_user(with_retry = true)
    # Cache the value the first time it's gotten.
    @cached_guest_user ||=
      User.find(session[:guest_user_id] ||= create_guest_user.id)
   rescue ActiveRecord::RecordNotFound # if session[:guest_user_id] invalid
     session[:guest_user_id] = nil
     guest_user if with_retry
  end

  private

  def after_sign_in_path_for(resource)
    url_for resource
  end

  # called (once) when the user logs in, insert any code your application needs
  # to hand off from guest_user to current_user.
  def logging_in
    if guest_user.game.present?
      guest_user.game.update(user_id: current_user.id)
      guest_user.player_boards.update_all(user_id: current_user.id)
    end

    current_user.save!
  end

  def create_guest_user
    # TODO: Come up with a better way to generate random emails
    name = "guest_#{SecureRandom.hex(10)}"
    random_email = "#{name}@example.com"

    user = User.new(
      name: name,
      guest: true,
      email: random_email,
    )

    user.save!(validate: false)

    session[:guest_user_id] = user.id

    user
  end

  protected

  def configure_permitted_parameters
    # Move these into the corresponding devise controllers..
    # I have done so for the Registrations Controller..
    # That way you are not overriding default params, and only appending the
    # ones you need.. in this case :name and :edit_password

    devise_parameter_sanitizer.permit(
      :sign_up,
      keys: %i[name email password password_confirmation],
    )
    devise_parameter_sanitizer.permit(
      :sign_in,
      keys: %i[login password password_confirmation],
    )
    devise_parameter_sanitizer.permit(
      :account_update,
      keys: %i[name email edit_password password_confirmation current_password],
    )
  end

  def from_play_game_path?
    referer_data[:controller].to_sym == :games &&
      referer_data[:action].to_sym == :show
  end
end
