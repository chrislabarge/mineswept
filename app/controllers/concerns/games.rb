module Games
  include TopPlayers

  def load_time
    @seconds = 0
    @time = 0

    if player_board_committed?
      if time = player_board.elapsed_time
        @time = Time.at(time).utc.strftime("%H:%M:%S")
        @seconds = time
      end
    end
  end

  def load_win_data
    load_time
    load_guest_user

    options = { items: 10 }

    @pagy_board, @top_board_players =
      pagy(find_top_players(board: @model.board), options)
  end

  def update_player_board
    player_board.player_touch if player_board_committed?
  end

  def player_board_committed?
    player_board.try(:board_id) == @model.board_id
  end

  def create_new_player_board
    current_or_guest_user.player_boards.
      create(board: @model.board)
  end

  def player_board
    current_or_guest_user.player_boards.last
  end

  def process_game_over
    if player_board&.playing?
      update_player_board
      player_board.update(status: :lost)
    end
  end

  def process_win
    player_board.update(status: :won)
  end
end
