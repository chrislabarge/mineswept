module TopPlayers
  def find_top_players(board: nil)
    if board
      User.joins(:player_boards).
        where(guest: false, player_boards: { board: board, status: :won }).
        or(User.joins(:player_boards).
                where(id: current_or_guest_user.id, player_boards: { board: board, status: :won })).
        order("player_boards.elapsed_time")
    else
      User.joins(:player_boards).
        where(guest: false, player_boards: { status: :won }).
        or(User.joins(:player_boards).
                where(id: current_or_guest_user.id, player_boards: { status: :won })).
        group("users.id").
        order("COUNT(player_boards.id) DESC")
    end
  end

  def load_guest_user
    user = current_or_guest_user
    @guest_user = user if user.guest?
  end
end
