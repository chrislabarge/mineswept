class GamesController < ApplicationController
  include Games

  before_action :load_model

  layout false, only: :update

  def show
    if player_board.try(:playing?)
      player_board.player_touch
    else
      @model.load_board
      @model.save
    end

    load_time
  end

  def game_over
    redirect_to(root_url) if @model.being_played?

    load_time
  end

  def win
    redirect_to(root_url) if @model.being_played?

    load_win_data
  end

  def update
    @model.assign_attributes(resource_params)
    @model.save

    if player_board_committed?
      update_player_board
    else
      create_new_player_board unless @model.game_over?
    end

    if @model.game_over?
      load_time
      process_game_over
      render inline: "Turbolinks.visit('#{game_over_path}')"
    elsif @model.won?
      load_time
      process_win
      render inline: "Turbolinks.visit('#{win_game_path}')"
    else
      render json: { alert: { success: "Success" } }, status: :ok
    end
  end

  def resource_params
    params.require(:game).permit(
      board_state: {},
    )
  end

  private

  def load_model
    user = current_or_guest_user

    return if @model = user.game

    @model = user.create_game(difficulty: :easy)
  end
end
