class GuestUsersController < ApplicationController
  include Games

  def show
    redirect_to win_game_path
  end

  def update
    @model = current_or_guest_user
    @model.assign_attributes(resource_params)

    if !@model.valid? && @model.errors.messages[:name].present?
      render_game_win
    else
      redirect_to new_user_registration_path(
        name: @model.name,
      )
    end
  end

  def render_game_win
    @guest_user = @model

    @model = current_or_guest_user.game

    load_win_data

    render template: "games/win"
  end

  def resource_params
    params.require(:user).permit(:name)
  end
end
