class NotificationsController < ApplicationController
  def index
    redirect_to crypto_path
  end

  def new
    @model = Notification.new(code: params[:code])
  end

  def create
    @model = Notification.new(resource_params)
    @model.save ? successful_create : unsuccessful_create
  end

  def edit
    load_model
  end

  def update
    load_model
    @model.assign_attributes(resource_params)
    @model.save ? successful_update : unsuccessful_update
  end

  def destroy
    load_model
    @model.destroy ? successful_destroy : unsuccessful_destroy
  end

  private

  def resource_params
    params.require(:notification).permit(
      :comparison,
      :price,
      :code,
    )
  end

  def load_model
    @model = Notification.find(params[:id])
  end
end
