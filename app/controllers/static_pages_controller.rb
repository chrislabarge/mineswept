class StaticPagesController < ApplicationController
  #skip_before_action :authenticate_user!

  def index
    if current_user
      redirect_to user_path(current_user)
    end
  end

  def coming_soon
  end

  def crypto
    @crypto_service = CryptoService.new
    @notifications = Notification.order(:created_at)
    CryptoJob.perform_later
  end

  def day_game
    render :coming_soon
  end

  def messages
    render :coming_soon
  end
end
