class TopPlayersController < ApplicationController
  include TopPlayers

  def index
    load_parent
    load_guest_user
    @pagy, @collection = pagy(find_top_players(board: @parent))
  end

  def load_parent
    return unless id = params[:board_id]

    @parent = Board.find(id)
  end
end
