class UsersController < ApplicationController
  def show
    load_model
  end

  def update
    load_model

    @model.assign_attributes(resource_params)

    @model.save ? successful_update : unsuccessful_update
  end

  def successful_update
    return super unless playing_game?

    redirect_to(play_game_url)
  end

  def playing_game?
    from_play_game_path?
  end

  private

  def load_model
    @model = current_or_guest_user
  end

  def resource_params
    params.require(:user).permit(
      game_attributes: [
        :id,
        :difficulty,
      ],
    )
  end
end
