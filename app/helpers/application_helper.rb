module ApplicationHelper
  include Pagy::Frontend

  def guest_user?
    current_or_guest_user.guest?
  end

  def current_resource
    # rubocop:disable Rails/HelperInstanceVariable
    @model
    # rubocop:enable Rails/HelperInstanceVariable
  end

  # TODO - This is a hack. I am pretty sure I need to enact the measures from url
  # https://github.com/heartcombo/devise/wiki/How-To:-Override-build_resource(%7B%7D)
  # I believe this is why I have to explisitly set the errors afterwords.
  def new_user_registration(user)
    user.assign_attributes(name: params[:name])

    user
  end

  def form_options(options)
    options
  end

  def field_wrapper_for(object, attr, no_label: nil, &block)
    errors = []

    if object.present?
      errors = object.errors.messages[attr]
    end

    render "shared/field_wrapper", errors: errors, no_label: no_label, &block
  end

  def centered_container(opt = {}, &block)
    render "shared/centered_container", opt, &block
  end

  def field_wrapper(options = {})
    default = "field"

    return default unless options[:no_label]

    default + " no-label"
  end

  def active_nav_class(path, active_class)
    return active_class + " text-gray-100" if current_page?(path)

    "text-gray-500"
  end

  def home_path
    if current_user
      user_path(current_user)
    else
      root_path
    end
  end
end
