module BoardHelper
  def spot_revealed?(game, y, x)
    return false unless row = game.board_state[y]

    row[x].present?
  end

  def input_name(y, x)
    "board_state[#{y}][#{x}]"
  end

  def default_data_options(is_spot_revealed, value)
    { revealed: is_spot_revealed.to_s,
      value: value,
      target: "board.spot" }
  end

  def form_btn_type(btn_is_bomb)
    btn_is_bomb ? :button : :submit
  end

  def spot_text(board_revealed, spot_revealed, value)
    blank = "&nbsp;"

    return blank unless board_revealed || spot_revealed

    value || blank
  end
end
