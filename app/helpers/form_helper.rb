module FormHelper
  def placeholders
    {
      email: "email@example.com",
      password: "************",
      login: "Username or Email",
    }
  end


  def dropdown_wrapper(opt = {}, &block)
    render "shared/dropdown_wrapper", opt, &block
  end
end
