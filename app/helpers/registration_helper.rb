module RegistrationHelper
  def minimum_password_length
    6
  end

  def change_password_toggle_label
    "I would like to change my Current Password"
  end
end
