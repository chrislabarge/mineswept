module ToggleHelper
  def toggle_switch(&block)
    content_tag(:div, class: toggle_wrapper_class) do
      capture(&block) + content_tag(:label, nil, class: toggle_label_class)
    end
  end

  def toggle_checkbox_class
    %w(
      toggle-checkbox
      absolute
      block
      w-6 h-6
      rounded-full
      bg-white border-4
      appearance-none
      cursor-pointer
    )
  end

  def toggle_wrapper_class
    %w(
      relative
      inline-block
      w-10 mr-2
      align-middle
      select-none
      transition
      duration-200
      ease-in
    )
  end

  def toggle_label_class
    %w(
      toggle-label
      block
      overflow-hidden
      h-6
      rounded-full
      bg-gray-300
      cursor-pointer
    )
  end
end
