module TopPlayersHelper
  def awards(place)
    default = "fas fa-award text-pink-500 mr-2 text-2xl"
    [
      "fas fa-trophy text-yellow-400 mr-2 text-2xl",
      "fas fa-trophy text-purple-200 mr-2 text-2xl",
      "fas fa-trophy text-yellow-700 mr-2 text-2xl",
      "fas fa-medal text-blue-400 mr-2 text-2xl",
      "fas fa-medal text-green-400 mr-2 text-2xl",
      "fas fa-medal text-purple-500 mr-2 text-2xl",
      default,
    ][place] || default
  end
end
