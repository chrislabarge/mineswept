module UserHelper
  def user_display_name(user = nil)
    user ||= current_user

    return "Guest User" unless user

    user.name
  end
end
