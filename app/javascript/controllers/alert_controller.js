import { Controller } from "stimulus";

export default class extends Controller {
  static targets = ["temp", "fieldError", "toast"]

  connect() {
    if (this.hasToastTarget) {
      this.processToastAlert(this.toastTarget)
    }

    if (this.hasTempTarget) {
      this.processTempAlert(this.tempTarget);
    }

    if (this.hasFieldErrorTarget) {
      this.initFieldError();
    }
  }

  // This allows for injecting
  processToastAlert(alert) {
    if (alert.parentElement.id === "toast_alerts") { return }

    alert.remove()

    this.toastContainer().appendChild(alert)
  }

  // Refactor this into a shared controller
  toastContainer() {
    return document.querySelector("#toast_alerts");
  }

  processTempAlert(alert) {
    setTimeout(() => {
      this.close(null, alert);
    }, 3000);
  }

  initFieldError() {
    this.fieldErrorTargets.forEach((error) => {
      const input = error.closest(".field-section").querySelector("input");

      input.addEventListener("input", () => {
        if (input.value === "") {
          error.remove();
        }
      });
    });
  }

  close(_event, alert = null) {
    this.findAlert(alert).querySelector(".close").click();
  }

  remove(_event, alert = null) {
    // TODO: refactor this so it waits for ".visible?" - to prevent from dead
    // time I use this here because it essentially waits for the animation to
    // complete
    setTimeout(() => {
      this.findAlert(alert).remove();
    }, 1000);
  }

  findAlert(alert) {
    return alert || this.element
  }
}
