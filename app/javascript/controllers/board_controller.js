import { Controller } from "stimulus"
import FastClick from "fastclick";

export default class extends Controller {
  static targets = [
    "spot",
    "row",
    "bomb",
    "winForm",
    "gameOverForm",
    "timer"
  ]

  initialize() {
    this.initBoard();
    this.initFastClick();

    console.log(this.board);
    console.log(this.playableSpotLength);
    console.log(this.spotTargets.length)
  }

  connect() {
    console.log("Board Connected")

    this.initTimer();
  }

  flag(event) {
    event.preventDefault()

    const spot = event.currentTarget.querySelector("button")

    if (event.currentTarget.dataset.revealed === "true") { return }

    if (event.currentTarget.dataset.flagged === "true") {
      delete event.currentTarget.dataset.flagged
      spot.textContent = " "
    } else {
      event.currentTarget.dataset.flagged = "true"
      spot.textContent = this.data.get("flag");
    }
  }

  reveal(event, spot = null) {
    spot = spot || event.currentTarget

    this.revealSpot(spot)

    if (this.isWinner()) {
//      this.renderWin();
    }

    this.startTimer();
  }

  initTimer() {
    if (this.element.dataset.revealed !== "true") {
      if (this.timerTarget.innerText !== "0") {
        this.startTimer();
      }
    }
  }

  startTimer() {
    if (this.timer == null) {
      setInterval(() => { this.incrementTime() }, 1000)
      this.timer = true
    }
  }

  incrementTime() {
    this.seconds ++
    this.timerTarget.textContent = this.seconds
  }

 // gameOver() {
 //   this.gameOverFormTarget.querySelector("[type='submit']").click();
 // }

  revealSpot(spot) {
    var val = spot.dataset.value

    spot.dataset.revealed = "true"
    spot.querySelector("input").disabled = false

    if (val) {
      spot.querySelector("button").textContent = val
    } else {
      this.revealQue.push(spot)
      this.recursiveReveal(spot)
    }
  }

  recursiveReveal(el) {
    var elCoords = this.getCoords(el)
    this.coords.forEach((coord) => {
      var spotCoords = [elCoords[0] + coord[0], elCoords[1] + coord[1]]

      if (this.isOut(spotCoords)) {
        return true
      } else {
        el = this.board[spotCoords[0]][spotCoords[1]]

        if (this.canReveal(el)) {
          this.reveal(null, el)
        }
      }
    })
  }

  canReveal(el) {
    return !this.revealQue.includes(el) && el.dataset.revealed !== "true"
  }

  getCoords(el) {
    var coords = []
    var index = 0

    this.board.forEach((row) => {
      var arr = Array.from(row)
      if (arr.find((node) => node === el)) {
        coords[0] = index
        coords[1] = arr.indexOf(el);
      }

      index++
    })

    return coords
  }

  isOut(coord) {
    var y = coord[0]
    var x = coord[1]

    return (y < 0 || y >= this.heightLength || x < 0 || x >= this.widthLength)
  }

  initBoard() {
    this.coords = JSON.parse(this.data.get("coords"));
    this.revealQue = [];
    this.board = [];
    this.boardId = this.data.get("id")
    this.seconds = this.data.get("seconds");

    this.rowTargets.forEach((row) => {
      this.board.push(row.querySelectorAll("[data-target*='board.spot']"))
    })

    this.playableSpotLength = this.element
      .querySelectorAll("[data-action*='board#reveal']").length

    this.heightLength = this.board.length;
    this.widthLength = this.board[0].length;
  }

  initFastClick() {
    if ('addEventListener' in document) {
      document.addEventListener('DOMContentLoaded', function() {
        FastClick.attach(this.element);
      }, false);
    }
  }

  getRevealedSpotLength() {
    return this.element.querySelectorAll("[data-revealed='true']").length
  }

  renderWin() {
    console.log("You Win!")

    this.winFormTarget.querySelector("[type='submit']").click();
  }

  isWinner() {
    return (this.playableSpotLength == this.getRevealedSpotLength())
  }
}
