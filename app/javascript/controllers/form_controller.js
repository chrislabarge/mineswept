import { Controller } from "stimulus";

export default class extends Controller {
  static targets = ["form", "state"]

  initialize() {
    this.setForm();
  }

  submit() {
    Rails.fire(this.form, 'submit')
  }

  loading() {
    this.stateTarget.innerHTML = "<i class='fas fa-spinner fa-spin'></i>"
  }

  success() {
    this.stateTarget.innerHTML = "<i class='far fa-check-circle'></i>"
  }

  setForm() {
    if (this.hasFormTarget) {
      this.form = this.formTarget
    } else {
      this.form = this.element
    }
  }
}
