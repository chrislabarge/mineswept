import { Controller } from "stimulus";

export default class extends Controller {
  static targets = ["content", "input"]

  connect() {
    this.content = document.querySelector(this.data.get("content-selector"));
  }

  toggle(event) {
    if (this.hasContentTarget) {
      this.contentTargets.forEach((content) => {
        this.toggleContent(content);
      });
    }

    if (this.hasInputTarget) {
      this.inputTargets.forEach((input) => {
        this.toggleInput(null, input);
      });
    }

    if (this.content) {
      this.toggleContent(this.content);
    }
  }

  toggleInput(event, input = null) {
    input = input || event.currentTarget;
    input.disabled = !input.disabled
  }

  toggleContent(content) {
    if (content.style.display === "none") {
      if (content.dataset.display === "inline") {
        content.style.display = "inline";
      } else {
        content.style.display = "block";
      }
    } else {
      content.style.display = "none";
    }
  }
}
