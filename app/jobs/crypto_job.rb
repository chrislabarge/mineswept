class CryptoJob < ApplicationJob
  queue_as :default

  after_perform do |_job|
    if Notification.any?
      self.class.set(wait: 1.minute).perform_later
    end
  end

  def perform(*args)
    ExchangeRate.create(
      coinbase: CryptoService.new.rates,
    )

    Notification.all.each do |notification|
      current_price = ExchangeRate.current_price(notification.code).to_d

      notify = if notification.comparison.to_sym == :above
                 current_price > notification.price.to_d
               else
                 current_price < notification.price.to_d
               end

      next unless notify

      CryptoNotificationMailer.notify(notification, current_price).deliver

      notification.destroy
    end
  end
end
