class ExportRatesJob < ApplicationJob
  queue_as :default

  def perform(*args)
    while ExchangeRate.count.positive?
      collection = ExchangeRate.order(:created_at).limit(200)
      export = ExportedRate.build(collection)

      if export.save
        collection.destroy_all
      else
        raise(StandardError.new("Export Error"))
      end
    end
  end
end
