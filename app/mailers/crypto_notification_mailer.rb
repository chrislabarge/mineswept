class CryptoNotificationMailer < ApplicationMailer
  default :from => ENV["EMAIL"]

  def notify(notification, current_price)
    @notification = notification
    @current_price = current_price
    mail( :to => "chrislabargedev@gmail.com",
         :subject => "#{notification.code} has gone #{notification.comparison} #{notification.price}" )
  end
end
