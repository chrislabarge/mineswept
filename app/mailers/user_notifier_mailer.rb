class UserNotifierMailer < ApplicationMailer
  default :from => ENV["EMAIL"]

  def send_signup_email(user)
    @user = user
    mail( :to => @user.email,
    :subject => "Thanks for signing up with #{ENV["APP_NAME"]}" )
  end
end
