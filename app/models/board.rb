class Board < ApplicationRecord
  has_many :player_boards, inverse_of: :board, dependent: :restrict_with_error
  has_many :games, dependent: :restrict_with_error

  def self.where_difficulty(difficulty)
    obj = BoardGenerator.new(difficulty: difficulty)
    size = obj.size

    where(
      width: size,
      width: size,
      bomb_count: obj.get_bomb_count,
    )
  end

  def won?(state)
    playable_spots_count == state.sum do |_row_index, row|
      row.count
    end
  end

  def playable_spots_count
    spot_count - bomb_count
  end

  def spot_count
    width * height
  end

  def game_over?(state)
    bomb_coords.each do |coord|
      y = coord[0]
      x = coord[1]

      return true if state[y] && state[y][x].present?
    end

    false
  end

  def bomb_value
    @bomb_value ||= BoardGenerator.new.bomb
  end

  def bomb_coords
    rows.map do |row_index, row|
      bombs = []
      row.each do |spot_index, spot|
        bombs << [row_index, spot_index] if spot.to_sym == bomb_value.to_sym
      end

      bombs
    end.flatten(1)
  end
end
