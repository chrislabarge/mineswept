class BoardGenerator
  attr_accessor :board
  attr_accessor :width_length
  attr_accessor :height_length
  attr_accessor :bomb_count
  attr_accessor :difficulty

  # TODO - Pass in the difficult and have a standard to create the dimensions and bomb count based on the difficulty
  def initialize(difficulty: :easy)
    @difficulty = difficulty.to_sym
    set_dimensions
  end

  def set_dimensions
    @width_length = size
    @height_length = size
    @bomb_count = get_bomb_count
  end

  def size
    case difficulty.to_sym
    when :easy
      10
    when :medium
      13
    when :hard
      17
    end
  end

  def get_bomb_count
    case difficulty.to_sym
    when :easy
      9
    when :medium
      19
    when :hard
      25
    end
  end

  def generate
    Board.create(
      rows: create_new_board,
      width: width_length,
      height: height_length,
      bomb_count: bomb_count,
    )
  end

  def create_new_board
    @board = {}

    height_length.times { |i| board[i.to_s] = {} }

    bomb_count.times do
      plant_bomb
    end

    board
  end

  def plant_bomb
    coords = random_coords

    if board[coords[0]][coords[1]] == bomb
      plant_bomb
    else
      board[coords[0]][coords[1]] = bomb
      populate_touching_spots(coords[0], coords[1])
    end
  end

  def random_coords
    row = rand(0..(height_length - 1))
    col = rand(0..(width_length - 1))

    [row.to_s, col.to_s]
  end

  def populate_touching_spots(y, x)
    touching_coords.each do |coord|
      spot_y = (y.to_i + coord[0]).to_s
      spot_x = (x.to_i + coord[1]).to_s

      next if out? spot_y, spot_x
      next if bomb? spot_y, spot_x

      board[spot_y][spot_x] = (board[spot_y][spot_x].to_i + 1).to_s
    end
  end

  def bomb?(y, x)
    board[y][x] == bomb
  end

  def out?(y, x)
    y = y.to_i
    x = x.to_i

    y < 0 || y >= height_length || x < 0 || x >= width_length
  end

  # This is the coordinates for all the spots touching a single spot
  def touching_coords
    [
      [-1, -1],
      [-1, 0],
      [-1, 1],
      [1, 0],
      [0, -1],
      [0, 1],
      [1, -1],
      [1, 1],
    ]
  end

  def bomb
    :B
  end
end
