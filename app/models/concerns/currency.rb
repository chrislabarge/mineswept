module Currency
  extend ActiveSupport::Concern

  TRADABLE = {
      BTC: "Bitcoin",
      BCH: "Bitcoin Cash",
      ETH: "Ethereum",
      LTC: "Litecoin",
      ETC: "Ethereum Classic",
      UNI: "Uniswap",
      ADA: "Cardano",
      LINK: "Chainlink",
      USDC: "USD Coin",
      XLM: "Stellar Lumens",
      WBTC: "Wrapped Bitcoin",
      FIL: "Filecoin",
      AAVE: "Aave",
      ATOM: "Cosmos",
      ALGO: "Algorand",
      DAI: "Dai",
      SNX: "Synthetic Network Token",
      GRT: "The Graph",
      MKR: "Maker",
      COMP: "Compound",
      MATIC: "Polygon",
      ZEC: "Zcash",
      BAT: "Basic Attention Token",
      UMA: "UMA",
      MANA: "Decentraland",
      BNT: "Bancor Network Token",
      YFI: "yearn.finance",
      ZRX: "Ox",
      REN: "REN",
      OMG: "OMG Network",
      CGLD: "Celo",
      LRC: "Loopring",
      KNC: "Kyber Network",
      SKL: "SKALE",
      BAL: "Balancer",
      BAND: "Band Protocol",
      OXT: "Orchid",
      CVC: "Civic",
      NU: "Nu Cypher",
      NMR: "Numeraire",
      STORJ: "Storj",
      ANKR: "Ankr"
    }

  included do
    def self.dollar_price(rate)
      (1 / rate.to_d).round(4)
    end
  end

  def dollar_price(*args)
    self.class.dollar_price(*args)
  end
end
