require 'coinbase/wallet'

class CryptoService
  include Currency

  def initialize
    @client = new_client
  end

  def spot_price(options = {})
    @client.spot_price(options)
  end

  def tradable_currencies
    Currency::TRADABLE.map do |code, name|
      price = dollar_price(rates[code.to_s])
      ["#{code} - #{name}", price, code]
    end
      .sort_by { |arr| arr[1] }
      .reverse
  end

  def rates
    @rates ||= @client.exchange_rates["rates"]
  end

  private

  def currencies
    @client.currencies
  end

  def new_client
    Coinbase::Wallet::Client.new(
      api_key: ENV["CB_KEY"],
      api_secret: ENV["CB_SECRET"],
    )
  end
end
