class ExchangeRate < ApplicationRecord
  include Currency

  def self.current_price(code)
    rate = ExchangeRate.order(:created_at).last.coinbase[code.to_s]
    dollar_price(rate)
  end
end
