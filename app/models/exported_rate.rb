class ExportedRate < ApplicationRecord
  include Currency
  has_one_attached :spreadsheet

  after_create :export
  attr_accessor :new_sheet

  class << self
    attr_accessor :exchange_rates
  end

  def self.build(exchange_rates)
    @exchange_rates = exchange_rates
    start = exchange_rates.first.try(:created_at)
    ending = exchange_rates.last.try(:created_at)
    new(
      file_name: "exchange_rates_#{start}-through-#{ending}.xlsx",
      new_sheet: build_sheet,
    )
  end

  def export
    spreadsheet.attach(
      io: @new_sheet.stream,
      filename: file_name,
    )
  end

  def self.build_sheet
    workbook = SpreadsheetWorkbook.new
    worksheet = workbook.worksheets[0]

    add_header_to worksheet
    add_rows_to(worksheet)

    workbook
  end

  private

  def self.add_rows_to(worksheet)
    @exchange_rates.each_with_index do |exchange_rate, index|
      index += 1
      insert_exchange_rate_row(worksheet, exchange_rate, index)
    end
  end

  def self.insert_exchange_rate_row(worksheet, exchange_rate, index)
    worksheet.add_cell(index, 0, exchange_rate.created_at)
    exchange_rate.coinbase.values.each_with_index do |rate, column_index|
      column_index += 1
      worksheet.add_cell(index, column_index, rate)
    end
  end

  def self.add_header_to(worksheet)
    worksheet.add_cell(0, 0, "Date-Time")
    @exchange_rates.last.coinbase.keys.each_with_index do |code, index|
      worksheet.add_cell(0, index + 1, code.to_s)
    end
  end
end
