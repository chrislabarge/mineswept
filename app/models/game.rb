class Game < ApplicationRecord
  belongs_to :board, optional: true
  belongs_to :user, inverse_of: :game

  enum difficulty: [:easy, :medium, :hard]

  before_validation :set_difficulty

  def clear_state
    self.board_state = {}
  end

  def difficulty=(val)
    super(val)
    return if user.blank?

    if obj = user.player_boards.last
      obj.update(status: :lost) if obj.playing?
    end
  end

  def load_board
    clear_state

    self.board =
      Board.where_difficulty(difficulty).
        where.not(id: user.played_board_ids).first ||

      BoardGenerator.new(difficulty: difficulty).generate
  end

  def won?
    board.won?(board_state)
  end

  def lost?
    board.game_over?(board_state)
  end

  def being_played?
    user.playing_game?
  end

  def game_over?
    board.game_over?(board_state)
  end

  def percent_complete
    return 0.0 unless board_state.present?

    decimal =
      board_state.sum { |_, spots| spots.count }.to_f / board.spot_count.to_f

    (decimal * 100).round
  end

  private

  def set_difficulty
    return if difficulty.present?

    self.difficulty = :easy
  end
end
