class PlayerBoard < ApplicationRecord
  belongs_to :board, inverse_of: :player_boards
  belongs_to :user, inverse_of: :player_boards

  enum status: [:won, :lost]

  after_create :ensure_only_playing

  scope :played, -> { where.not(status: nil) }
  scope :won, -> { where(status: :won) }
  scope :lost, -> { where(status: :lost) }

  def player_touch
    time = elapsed_time.to_i
    time += (Time.now.utc - updated_at.utc).to_i

    self.elapsed_time = time

    save
  end

  def playing?
    status == :playing
  end

  def lost?
    status.to_sym == :lost
  end

  def status
    return :playing if super.nil?

    super
  end

  def status=(val)
    val = nil if val.try(:to_sym) == :playing

    super(val)
  end

  def ensure_only_playing
    user.player_boards.
      where(status: nil).
      where.not(id: id).
      update_all(status: :lost)
  end
end
