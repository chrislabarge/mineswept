require "rubyXL/convenience_methods/worksheet"
require "rubyXL/convenience_methods/cell"
require "rubyXL/convenience_methods/color"
require "rubyXL/convenience_methods/font"
require "rubyXL/convenience_methods/workbook"

class SpreadsheetWorkbook
  def self.new
    RubyXL::Workbook.new
  end
end
