class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :player_boards, inverse_of: :user, dependent: :destroy
  has_one :game, inverse_of: :user, dependent: :destroy

  attr_accessor :edit_password
  attr_writer :login

  after_create :notify_new_user

  delegate :difficulty, to: :game

  accepts_nested_attributes_for :game

  validates :name, {
    presence: true,
    format: {
      with: /\A[a-z\d][a-z\d-]*[a-z\d]\z/i,
      message: "Username format is invalid",
    },
    uniqueness: { case_sensitive: false },
  }

  validates_length_of :name, minimum: 5, maximum: 25, allow_blank: false

  scope :guest, -> { where(guest: true) }

  def login
    @login || self.name || self.email
  end

  def played_board_ids
    player_boards.pluck(:board_id)
  end

  def win_count
    player_boards.won.count
  end

  def playing_game?
    player_boards.last.try(:playing?)
  end

  def current_board
    game.board
  end

  def lost_count
    player_boards.lost.count
  end

  def play_count
    player_boards.played.count
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup

    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(name) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:name) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end

  private

  def notify_new_user
    return if guest?

    UserNotifierMailer.send_signup_email(self).deliver
  end
end
