Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: "users/registrations" }

   root to: "static_pages#index"

   get "crypto", to: "static_pages#crypto"

   get :day_game, to: "static_pages#day_game"
   get :messages, to: "static_pages#messages"
   get :play_game, to: "games#show"
   get :win_game, to: "games#win"
   get :game_over, to: "games#game_over"

   resources :games, only: [ :show, :update ]
   resources :guest_users, only: [ :show, :update ]

   resources :top_players, only: :index

   resources :boards, only: [ :index ] do
     resources :top_players, only: :index
   end

   resources :users, only: [:update, :show]

   # Cryto
   resources :notifications
end
