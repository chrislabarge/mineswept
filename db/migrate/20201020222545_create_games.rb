class CreateGames < ActiveRecord::Migration[6.0]
  def change
    create_table :games do |t|
      t.references :board, null: false, foreign_key: true
      t.jsonb :board_state, null: false, default: {}
      t.integer :difficulty
      t.jsonb :board_que, null: false, default: []

      t.timestamps
    end
  end
end
