class AllowNullBoardInGames < ActiveRecord::Migration[6.0]
  def change
    change_column_null :games, :board_id, true
  end
end
