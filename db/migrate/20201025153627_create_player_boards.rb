class CreatePlayerBoards < ActiveRecord::Migration[6.0]
  def change
    create_table :player_boards do |t|
      t.references :board, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :elapsed_time
      t.integer :status

      t.timestamps
    end
  end
end
