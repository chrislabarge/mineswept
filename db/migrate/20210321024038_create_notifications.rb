class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.money :price
      t.string :comparison
      t.string :code

      t.timestamps
    end
  end
end
