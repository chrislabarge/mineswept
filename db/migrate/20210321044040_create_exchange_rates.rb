class CreateExchangeRates < ActiveRecord::Migration[6.0]
  def change
    create_table :exchange_rates do |t|
      t.jsonb :coinbase, null: false, default: {}

      t.timestamps
    end
  end
end
