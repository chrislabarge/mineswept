class CreateExportedRates < ActiveRecord::Migration[6.0]
  def change
    create_table :exported_rates do |t|
      t.string :file_name

      t.timestamps
    end
  end
end
