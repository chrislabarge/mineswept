namespace :boards do
  desc "destroy the Boards"

  task clear: :environment do
    Board.destroy_all
  end
end
