namespace :crypto do
  desc "clear notifications"

  task clear_notifications: :environment do
    CryptoJob.perform_later
  end

  task export_rates: :environment do
    ExportRatesJob.perform_later
  end
end
