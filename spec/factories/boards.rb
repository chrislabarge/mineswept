FactoryBot.define do
  factory :board do
    rows { {} }
    width { 10 }
    height { 10 }
  end
end
