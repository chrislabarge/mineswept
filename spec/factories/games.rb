FactoryBot.define do
  factory :game do
    user
    board { nil }
    board_state { {} }
    difficulty { 0 }
    board_que { [] }
  end
end
