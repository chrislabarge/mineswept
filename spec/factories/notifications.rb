FactoryBot.define do
  factory :notification do
    price { "100.00" }
    comparison { "active" }
    code { "BTC" }
  end
end
