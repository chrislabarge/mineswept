FactoryBot.define do
  factory :player_board do
    board
    user
    elapsed_time { 1 }
    status { nil }
  end
end
