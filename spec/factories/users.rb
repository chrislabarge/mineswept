FactoryBot.define do
  factory :user do
    sequence(:name) do |i|
      Faker::Internet.username(
        specifier: 6,
        separators: ["-"]
      ) + i.to_s
    end
    email { Faker::Internet.email }
    password { "Password1" }
    guest { false }
  end
end
