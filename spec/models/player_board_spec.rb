require "rails_helper"

RSpec.describe PlayerBoard, type: :model do
  describe "before_create" do
    subject!(:player_board) { build(:player_board) }

    let!(:user) { player_board.user }

    context "when a user has an existing PlayerBoard" do
      let!(:existing) { create(:player_board, user: user) }

      context "when the existing status is 'PLAYING'" do
        before do
          existing.update(status: :playing)

          player_board.save
        end

        it "updates the status of the existing" do
          expect(existing.reload.status.try(:to_sym)).to eq :lost
        end

        it "gives the instance default status of playing" do
          expect(player_board.reload.status).to eq :playing
        end
      end
    end
  end
end
