module AlertSpecHelper
  def successful_message
    "Successfully"
  end

  def expect_toast_alert(successful_message)
    expect(page).to have_content(successful_message)
  end
end
