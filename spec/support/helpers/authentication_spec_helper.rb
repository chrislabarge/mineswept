module AuthenticationSpecHelper
  def authenticate_user(user = nil, with_driver: false)
    user ||= create(:user)

    if with_driver
      visit new_user_session_path

      fill_in_form(user)

      submit_sign_in_form
    else
      # This allows for successful load of user.settings
      # And any initializers
      user = User.find(user.id)

      login_as(user, scope: :user)
    end

    user
  end

  def fill_in_form(user)
    if page.has_css?("label", text: "Login")
      fill_in "Login", with: user.name || user.email
    else
      fill_in "Username", with: user.name
      fill_in "Email", with: user.email
    end

    fill_in "Password", with: user.password

    confirmation_label_text = "Password confirmation"

    if page.has_css?("label", text: confirmation_label_text)
      fill_in confirmation_label_text, with: user.password
    end
  end

  def sign_out
    click_on "Sign Out"
  end

  def authentication_messages
    @devise_messages ||= YAML.load_file("config/locales/devise.en.yml")

    @devise_messages.dig("en", "devise")
  end
end
