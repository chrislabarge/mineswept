module BoardSpecHelper
  def click_all_playable_spots
    count = find_playable_spots.count

    count.times do |_index|
      if all("h1", text: "You Win").count == 1
        return
      elsif all("h1", text: "Game Over").count == 1
        raise StandardError.new("Unexpected Game Over")
      else
        find(playable_spot_selector, match: :first).click
      end

    rescue Capybara::ElementNotFound => e
      #save_screenshot("foo.png", full: true)

      puts e.inspect
    end
  end

  def loaded_board(user)
    user.game.reload.board
  end

  def touching_spots(spot)
    board = {}

    all("[data-target='board.row']").each_with_index do |row, index|
      board[index] = row.all("[data-target='board.spot']")
    end

    spots = []
    spotCoord = []

    board.each do |index, row|
      if row.find(spot)
        spotCoord << index
        spotCoord << row.index(spot)

        break
      end
    end

    BoardGenerator.new.touching_coords.each do |coord|
      touching_spot_coord =
        [spotCoord[0] + coord[0], spotCoord[1] + coord[1]]

      if row = board[touching_spot_coord[0]]
        if spot = row[touching_spot_coord[1]]
          spots << spot
        end
      end
    end

    spots
  end

  def bomb_value
    BoardGenerator.new.bomb
  end

  def find_playable_spots
    all(playable_spot_selector)
  end

  def all_bomb_spots
    all(bomb_spot_selector)
  end

  def first_playable_spot
    find(playable_spot_selector, match: :first)
  end

  def playable_spot_selector
    "#{spot_selector}:not([data-revealed='true']):not([data-target*='board.bomb'])"
  end

  def spot_selector
    "[data-action*='board#reveal']"
  end

  def revealed?(spot)
    spot["data-revealed"] == "true"
  end

  def click_bomb
    first_bomb_spot.click

    sleep(0.25)
  end

  def first_bomb_spot
    find(bomb_spot_selector, match: :first)
  end

  def bomb_spot_selector
    "[data-value='💣']"
  end

  def board_revealed?
    find("[data-controller='board']")["data-revealed"] == "true"
  end

  def first_numeric_spot
    all("[data-value='1']").first || all("[data-value='2']").first
  end

  def find_timer
    find("[data-target='board.timer']")
  end
end
