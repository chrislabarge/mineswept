module SystemSpecHelper
  include ActionView::RecordIdentifier

  def page_header
    find('h1')
  end

  def board_selector
    "[data-controller='board']"
  end

  def wait_for_js_init
    sleep(0.1)
  end

  def find_parent_element(element)
    element.find(:xpath, "..")
  end

  def submit_form(opt = {})
    find("[type='submit']", opt).click
  end

  def fill_in_trix_editor(content)
    formated = content.blank? ? "" : "<div>#{content}</div>"

    find(".trix-content", match: :first).set formated
  end

  def process_win_for(user)
    board = create(:player_board, status: :won, user: user).board

    user.game.update(board: board)

    visit win_game_path
  end
end
