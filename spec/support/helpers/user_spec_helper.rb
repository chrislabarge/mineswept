module UserSpecHelper
  def update_user_difficulty(difficulty: :medium)
    select(difficulty.to_s, match: :first)

    click_on "Update", match: :first
  end
end
