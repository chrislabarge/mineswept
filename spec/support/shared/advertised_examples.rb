RSpec.shared_examples "A Page with Advertised Features" do
  it "renders the advertised features" do
    I18n.t(:features).each do |feature|
      expect(page).to have_content feature.to_s
    end
  end
end
