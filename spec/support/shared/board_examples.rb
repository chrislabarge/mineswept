RSpec.shared_examples "A Playable Board" do
  it "renders the game board" do
    expect(page).to have_css(board_selector)
  end

  describe "saves state" do
    describe "after interacting with the board" do
      before do
        first_playable_spot.click
        sleep(2)
      end

      context "when navigating back and then forward in the browser" do
        let!(:current_board) { user.player_boards.last }
        let!(:current_state) { user.game.board_state }

        it "saves the state of the board" do
          current_seconds = elapsed_time_displayed

          go_back
          go_forward

          expect(user.player_boards.last).to eq current_board
          expect(user.game.board_state).to eq current_state
          expect(elapsed_time_displayed).to eq current_seconds
        end
      end

      context "when navigating away from the board and back" do
        let!(:current_board) { user.player_boards.last }
        let!(:current_state) { user.game.board_state }

        it "saves the state of the board" do
          current_seconds = elapsed_time_displayed

          visit("/")
          visit(play_game_path)

          expect(user.player_boards.last).to eq current_board
          expect(user.game.board_state).to eq current_state
          expect(elapsed_time_displayed).to eq current_seconds
        end
      end
    end
  end

  def elapsed_time_displayed
    find_timer.text
  end
end
