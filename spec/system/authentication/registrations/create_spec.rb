require "rails_helper"
require "./spec/support/shared/advertised_examples.rb"
require "./spec/support/helpers/authentication_spec_helper.rb"
require "./spec/support/helpers/board_spec_helper.rb"
require "./spec/support/helpers/alert_spec_helper.rb"

RSpec.describe "Authentication:Registration", type: :feature do
  include SystemSpecHelper
  include AuthenticationSpecHelper
  include AlertSpecHelper
  include BoardSpecHelper

  let!(:user) { build_stubbed(:user) }
  let(:model) { User.last }

  context "when a brand new user visits the sign up form" do
    before do
      visit new_user_registration_path
    end

    it_behaves_like("A Page with Advertised Features")

    context "when a guest user" do
      let(:guest_user) { User.guest.last }
      let(:user_count) { User.count }

      describe "successfully fills out the registration form" do
        before do
          fill_in_form(user)

          submit_form
        end

        it 'creates a registered user and destroy guest' do
          expect_toast_alert(successful_message)

          expect(User.count).to eq user_count
          expect(model).not_to eq guest_user
          expect(User.guest).to be_blank
        end
      end
    end

    it "register provides a password that is too short" do
      user.password = "Foo"

      fill_in_form(user)

      submit_form

      expect(page).to have_text(/too short/i)
    end

    it "register provides a non matching password" do
      fill_in_form(user)
      fill_in "Password", with: "non_matching_password"

      submit_form

      expect(page).to have_text(/doesn't match Password/i)
    end
  end

  context "when a guest user win a game", js: true do
    before do
      visit play_game_path

      process_win_for(User.last)
    end

    describe "they are able to sign up by ranking in Top Players" do
      let(:count) { User.count }
      let(:game_count) { Game.count }
      let(:player_board_count) { PlayerBoard.count }
      let!(:guest_data) do
        guest = User.guest.last

        { game: guest.game, player_boards: guest.player_boards }
      end

      it "renders the guest user win message" do
        expect(page).to have_content(I18n.t(:first_board_win_guest))
      end

      context "when hovering over the username input field" do
        it "renders the username format rules" do
          expect(page).to have_content(I18n.t(:first_board_win_guest))

          find("input#user_name", match: :first).hover

          I18n.t(:username_rules).each do |rule|
            expect(page).to have_content rule.to_s
          end
        end
      end

      context "when they choose an existing username" do
        let!(:existing_name) { create(:user).name }

        before do
          user.name = existing_name
          submit_name user
        end

        it "renders error" do
          expect(page).to have_text(/name has already been taken/i)
        end
      end

      context "when they choose an existing with UPPERCASED username" do
        let!(:existing_name) { create(:user).name }

        before do
          user.name = existing_name.upcase
          submit_name user
        end

        it "renders error" do
          expect(page).to have_text(/name has already been taken/i)
        end
      end

      context "when choose a name with white space" do
        before do
          user.name = "Some White Space"

          submit_name user
        end

        it "renders error" do
          expect(page).to have_text(/username format is invalid/i)
        end
      end

      context "when too many characters" do
        before do
          user.name = "TOOMANYCHARACRERSSSSSSSSSSSSSSSsdasdasdasdas"

          submit_name user
        end

        it "renders error" do
          expect(page).to have_text(/name is too long/i)
        end
      end

      context "when too few characters" do
        before do
          user.name = "isad"

          submit_name user
        end

        it "renders error" do
          expect(page).to have_text(/name is too short/i)
        end
      end

      context "when choose a name starting with dash" do
        before do
          user.name = "-Some-Name"

          submit_name user
        end

        it "renders error" do
          expect(page).to have_text(/username format is invalid/i)
        end
      end

      context "when choose a name ending with dash" do
        before do
          user.name = "Some-Name-"

          submit_name user
        end

        it "renders error" do
          expect(page).to have_text(/username format is invalid/i)
        end
      end

      context "when choose a name ending with special characters" do
        before do
          user.name = "Some-Name&@($"

          submit_name user
        end

        it "renders error" do
          expect(page).to have_text(/username format is invalid/i)
        end
      end

      context "when they submit a completed registration" do
        before do
          submit_name user

          fill_in_form(user)

          submit_form
        end

        it "creates a new user via initial guest user data" do
          expect_toast_alert(successful_message)

          expect(model.game.id).to eq guest_data[:game].id
          guest_data[:player_boards].each do |obj|
            expect(model).to eq obj.reload.user
          end
          # expect(model.player_board_ids).
          #  to eq guest_data[:player_boards].map(&:id)

          expect(User.count).to eq count
          expect(Game.count).to eq game_count
          expect(PlayerBoard.count).to eq player_board_count
        end
      end
    end
  end

  def submit_name(user)
    fill_in(:user_name, with: user.name, match: :first)

    submit_form(match: :first)
  end

  def successful_message
    authentication_messages.dig("registrations", "signed_up")
  end
end
