require "rails_helper"

require "./spec/support/helpers/authentication_spec_helper.rb"
require "./spec/support/helpers/alert_spec_helper.rb"

RSpec.describe "Authentication:Destroy User Registration", type: :feature do
  include AuthenticationSpecHelper
  include AlertSpecHelper

  let(:user) { create(:user) }

  before do
    authenticate_user(user)

    visit edit_user_registration_path
  end

  it "user destroys their registration", js: true do
    user_count = User.count

    accept_confirm do
      click_on("Delete Account")
    end

    expect_toast_alert(successful_message)

    expect(User.count).to eq user_count - 1
  end

  def successful_message
    authentication_messages.dig("registrations", "destroyed")
  end
end
