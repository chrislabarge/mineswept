require "rails_helper"
require "./spec/support/helpers/authentication_spec_helper.rb"
require "./spec/support/helpers/alert_spec_helper.rb"

RSpec.describe "Authentication:Update User Registration", type: :feature do
  include SystemSpecHelper
  include AuthenticationSpecHelper
  include AlertSpecHelper
  include RegistrationHelper

  let(:user) { create(:user) }
  let(:new_password) { "new_password" }
  let(:new_email) { "new@email.com" }

  before do
    authenticate_user(user)

    visit edit_user_registration_path
  end

  it "a user edits their email" do
    original_email = user.email

    fill_in_form(email: new_email, password: user.password)

    submit_form

    expect_toast_alert(successful_message)

    user.reload

    expect(user.email).not_to eq(original_email)
    expect(user.email).to eq(new_email)
  end

  it "user edits their password", js: true do
    fill_in_form(email: user.email, password: user.password)

    fill_in_new_password_form(new_password)

    submit_form

    expect_toast_alert(successful_message)
  end

  it "updates a user's email and password", js: true do
    fill_in_form(email: new_email, password: user.password)

    fill_in_new_password_form(new_password)

    submit_form

    expect_toast_alert(successful_message)
  end

  it "a user does not enter the current password" do
    fill_in_form(email: new_email, password: "")

    submit_form

    expect(page).to have_text(/can't be blank/i)
  end

  it "a user does not enter the new password fields", js: true do
    fill_in_form(email: user.email, password: user.password)

    toggle_new_password_fields

    fill_in "New Password", with: new_password

    submit_form

    expect(page).to have_text(/doesn't match Password/i)

    fill_in "New Password", with: ""
    fill_in "Confirm New Password", with: new_password

    submit_form

    expect(page).to have_text(/doesn't match Password/i)
  end

  pending "a user enters an invalid email" do
    invalid_email = "invalidemail"

    fill_in_form(email: invalid_email, password: user.password)

    submit_form

    pending("The response returns an error")

    expect(page).to have_text("Email is invalid")
  end

  def fill_in_form(email: nil, password: nil)
    fill_in "Email", with: email
    fill_in "Current Password", with: password
  end

  def fill_in_new_password_form(new_password)
    toggle_new_password_fields

    fill_in "New Password", with: new_password, wait: 3
    fill_in "Confirm New Password", with: new_password
  end

  def toggle_new_password_fields
    toggle_label = find("label", text: change_password_toggle_label)
    toggler = find_parent_element(toggle_label).find("input")

    toggler.click
  end

  def submit_form
    click_on "Update User"
  end

  def successful_message
    authentication_messages.dig("registrations", "updated")
  end
end
