require "rails_helper"

require "./spec/support/helpers/authentication_spec_helper.rb"
require "./spec/support/helpers/alert_spec_helper.rb"

RSpec.describe "Authentication:Create User Session", type: :feature do
  include SystemSpecHelper
  include AuthenticationSpecHelper
  include AlertSpecHelper

  context "when a user successfully signs in" do
    let!(:user) { create(:user) }

    it "logs a user in" do
      visit new_user_session_path

      fill_in_form(user)

      submit_form

      expect_toast_alert(successful_message)
    end

    it "with remember me checked, it permanently logs in the user", js: true do
      visit new_user_session_path

      fill_in_form(user)

      check("Remember me")

      submit_form

      expect(page.driver.cookies["remember_user_token"]).to be_present
      expect_toast_alert(successful_message)
    end

    it "user signs in with an invalid email" do
      user.name = nil
      invalid_email = "invalidemail"
      user.email = invalid_email

      visit new_user_session_path

      fill_in_form(user)

      submit_form

      expect_toast_alert(unsuccessful_message)
    end

    it "user signs in with wrong email" do
      user.name = nil
      user.email = "notmyemail@mail.com"

      visit new_user_session_path

      fill_in_form(user)

      submit_form

      expect_toast_alert(unsuccessful_message)
    end

    it "user signs in with wrong password" do
      user = create(:user)

      user.password = "foobar"

      visit new_user_session_path

      fill_in_form(user)

      submit_form

      expect_toast_alert(unsuccessful_message)
    end
  end

  def successful_message
    "Signed in successfully"
  end

  def unsuccessful_message
    "Invalid Login or password"
  end
end
