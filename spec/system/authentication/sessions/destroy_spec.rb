require "rails_helper"

require "./spec/support/helpers/authentication_spec_helper.rb"
require "./spec/support/helpers/alert_spec_helper.rb"

RSpec.describe "Authentication:Destroy User Session", type: :feature do
  include AuthenticationSpecHelper
  include AlertSpecHelper

  let(:user) { create(:user) }

  before do
    authenticate_user(user)

    visit root_path
  end

  it "user ends the current session" do
    sign_out

    expect_toast_alert(successful_message)
  end

  def successful_message
    authentication_messages.dig("registrations", "signed_out")
  end
end
