require "rails_helper"

require "./spec/support/helpers/authentication_spec_helper.rb"
require "./spec/support/helpers/top_players_spec_helper.rb"

RSpec.describe "Top Players : Index", type: :feature do
  include SystemSpecHelper
  include AuthenticationSpecHelper
  include TopPlayersSpecHelper

  context "when an authenticated user" do
    let!(:player_board) do
      create(:player_board, elapsed_time: 100, status: :won)
    end
    let!(:user) { player_board.user }
    let!(:board) { player_board.board }

    before do
      authenticate_user(user)
    end

    describe "visits the top players index" do
      context "when there are 1 top player" do
        before do
          visit(url_for([board, :top_players, only_path: true]))
        end

        it "renders the view header" do
          expect(page).to have_content("Top Players - Time")
        end

        it "renders the top players" do
          expect(page).to have_content(user.name)
          expect(page).to have_content("Time: #{player_board.elapsed_time}")
          expect(page).to have_content("#{user.win_count} Games Won")
          expect(page).to have_content("#{user.lost_count} Games Lost")
        end
      end

      context "when there is 2 top players" do
        let!(:another_player_board) do
          create(:player_board, board: board, elapsed_time: 99, status: :won)
        end
        let!(:another_user) { another_player_board.user }

        before do
          visit(url_for([board, :top_players, only_path: true]))
        end

        it "renders the view header" do
          expect(page).to have_content("Top Players - Time")
        end

        it "renders the user with less elapsed time first" do
          first_player = top_player_index.find("li", match: :first)

          expect(first_player).to have_content(another_user.name)
        end

        it "does not render the pagination" do
          expect(page).not_to have_content("Next")
        end
      end

      context "when there are 21 top board players" do
        before do
          21.times do
            create(:player_board,
                   elapsed_time: 99, board: board, status: :won)
          end

          visit(url_for([board, :top_players, only_path: true]))
        end

        it "renders the pagination", js: true do
          click_on("2")

          expect(page).to have_content(User.last.name)
        end
      end
    end
  end
end
