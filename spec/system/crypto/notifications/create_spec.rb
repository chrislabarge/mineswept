require "rails_helper"

RSpec.describe "Create notification", type: :feature do
  include SystemSpecHelper

  context "when an visitor" do
    describe "navigates to the crypto notifications" do
      before do
        visit crypto_path
      end

      context "when submitting a new notification form" do
        before do
          click_on "Notify", match: :first
          select "above"
          fill_in "Price", with: "100.00"
          click_on "Create Notification"
        end

        it "creates a notification" do
          expect(page)
            .to have_content("Successfully created the Notification")
        end
      end
    end
  end
end
