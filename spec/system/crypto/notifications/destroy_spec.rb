require "rails_helper"

RSpec.describe "Destroy notification", type: :feature, js: true do
  include SystemSpecHelper

  let!(:model) { create(:notification) }

  context "when an visitor" do
    describe "navigates to the crypto notifications" do
      before do
        visit crypto_path
      end

      context "when removing an existing notification" do
        before do
          accept_confirm do
            click_on "Destroy"
          end
        end

        it "destroys the notification" do
          expect(page)
            .to have_content("Successfully deleted the Notification")
        end
      end
    end
  end
end
