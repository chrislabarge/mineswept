require "rails_helper"

RSpec.describe "Update notification", type: :feature do
  include SystemSpecHelper

  let!(:model) { create(:notification) }

  context "when an visitor" do
    describe "navigates to the crypto notifications" do
      before do
        visit crypto_path
      end

      context "when editing an existing notification" do
        before do
          click_on "Edit", match: :first
          select "below"
          fill_in "Price", with: "200.00"
          click_on "Update Notification"
        end

        it "updates the notification" do
          expect(page)
            .to have_content("Successfully updated the Notification")
        end
      end
    end
  end
end
