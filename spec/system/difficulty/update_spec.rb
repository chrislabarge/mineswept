require "rails_helper"
require "./spec/support/shared/board_examples.rb"
require "./spec/support/helpers/board_spec_helper.rb"
require "./spec/support/helpers/user_spec_helper.rb"
require "./spec/support/helpers/authentication_spec_helper.rb"

RSpec.describe "Update Difficulty", type: :feature, js: true do
  include SystemSpecHelper
  include BoardSpecHelper
  include UserSpecHelper
  include AuthenticationSpecHelper

  context "when an authenticated user" do
    let!(:model) { create(:game) }
    let!(:user) { model.user }
    let!(:difficulty) { :medium }

    before do
      authenticate_user(user)
    end

    describe "updates their difficulty" do
      before do
        # for the sake of loading a board
        visit(play_game_path)
      end

      context "when there is no active game" do
        let!(:original_board) { loaded_board(user) }

        before do
          visit(user_path(user))

          update_user_difficulty(difficulty: difficulty)
        end

        it "updates the difficulty" do
          expect(user.reload.difficulty.to_sym).to eq difficulty
        end

        it "loads a new game board upon playing" do
          visit(play_game_path)

          expect(loaded_board(user)).not_to eq original_board
        end
      end

      context "when they have an active game" do
        let!(:player_board) { create(:player_board, user: user) }

        describe "and accepts the alert " do
          before do
            visit(user_path(user))

            accept_confirm(I18n.t(:current_game_warning)) do
              update_user_difficulty(difficulty: difficulty)
            end
          end

          it "updates the users difficulty" do
            expect(user.reload.difficulty.to_sym).to eq difficulty
          end

          it "commits playing board as loss" do
            expect(player_board.reload.lost?).to eq true
          end
        end

        describe "and dismisses the alert " do
          before do
            dismiss_confirm(I18n.t(:current_game_warning)) do
              visit(user_path(user))

              update_user_difficulty(difficulty: difficulty)
            end
          end

          it "does not update the users difficulty" do
            expect(user.reload.difficulty.to_sym).not_to eq difficulty
          end

          it "does NOT affect the playing board" do
            expect(player_board.reload.lost?).not_to eq true
          end
        end
      end

      context "when they are currently playing a game" do
        let!(:player_board) { create(:player_board, user: user) }

        before do
          visit(play_game_path)
        end

        describe "and accepts the alert " do
          it "redirects them to a new game board" do
            initial_spot_count = all(spot_selector).count

            accept_confirm(I18n.t(:current_game_warning)) do
              update_user_difficulty(difficulty: :medium)
            end

            expect(all(spot_selector).count).not_to eq initial_spot_count
          end
        end

        describe "and dismisses the alert " do
          it "redirects them to a new game board" do
            initial_spot_count = all(spot_selector).count

            dismiss_confirm(I18n.t(:current_game_warning)) do
              update_user_difficulty(difficulty: :medium)
            end

            expect(all(spot_selector).count).to eq initial_spot_count
          end
        end
      end
    end
  end
end
