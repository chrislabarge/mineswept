require "rails_helper"
require "./spec/support/shared/board_examples.rb"
require "./spec/support/helpers/user_spec_helper.rb"
require "./spec/support/helpers/board_spec_helper.rb"
require "./spec/support/helpers/authentication_spec_helper.rb"

RSpec.describe "Show Game", type: :feature, js: true do
  include SystemSpecHelper
  include BoardSpecHelper
  include UserSpecHelper
  include AuthenticationSpecHelper

  context "when an authenticated user" do
    let!(:model) { create(:game) }
    let!(:user) { model.user }

    before do
      authenticate_user(user)
    end

    context "when there is an existing board" do
      let!(:board) { BoardGenerator.new.generate }

      describe "and the user has not played it yet" do
        describe "navigates to the show" do
          before do
            visit(play_game_path)

            first_playable_spot.click
          end

          it 'assigns the board to the user' do
            expect(user.reload.current_board).to eq board
          end
        end
      end

      describe "and the user has already played it" do
        before do
          user.player_boards.create(
            elapsed_time: 2,
            board: board,
            status: :won,
          )
        end

        describe "navigates to the show" do
          before do
            visit(play_game_path)

            first_playable_spot.click
          end

          it 'creates a new board for the user' do
            expect(user.reload.current_board).not_to eq board
          end
        end
      end
    end

    describe "navigates to the show" do
      before do
        visit(play_game_path)
      end

      describe "scope this shared example" do
        it_behaves_like("A Playable Board")
      end

      context "when a spot has a numeric value" do
        describe "before the spot is clicked" do
          it "does not display the value" do
            expect(first_numeric_spot.text).to be_blank
          end
        end

        describe "after the spot is clicked" do
          it "displays the value" do
            first_numeric_spot.click

            expect(first_numeric_spot.text).not_to be_blank
          end
        end
      end

      context "when the spot is blank" do
        def first_blank_spot
          all("[data-target='board.spot']:not([data-value]").first
        end

        describe "before the spot is clicked" do
          it "does not display a value" do
            expect(first_blank_spot.text).to be_blank
          end
        end

        describe "after the spot is clicked" do
          before do
            first_blank_spot.click
          end

          it "does not display a value" do
            expect(first_blank_spot.text).to be_blank
          end

          it "reveals the spots touching" do
            revealed_spots = all("[data-revealed='true']")

            expect(revealed_spots.count > 1).to eq true
            # TODO : This is passing here and there.. I am trying to record the
            # logic for front end cascading reveals to test that it properly
            # worked. Because of the flakeyness I decided to comment it out
            # rather than leave pending
            #
            # I could also try and compare the `revealed_spots` array with
            # `touching_spots` and make sure they are all included
            #
            # touching_spots(first_blank_spot).each do |spot|
            #  expect(revealed?(spot)).to eq true
            # end
          end

          it "starts the timer" do
            sleep(1)

            timer = find("[data-target='board.timer']")

            expect(timer).not_to have_content(0)
          end

          context "when changing the difficulty" do
            let!(:og_player_board) { PlayerBoard.last }
            let!(:og_board_status) { og_player_board.status.to_sym }

            before do
              visit(user_path(user))

              accept_confirm(I18n.t(:current_game_warning)) do
                update_user_difficulty(difficulty: :medium)
              end

              visit(play_game_path)
            end

            it "updates the state of the original player board" do
              current_status = og_player_board.reload.status.to_sym

              expect(current_status).not_to eq og_board_status
              expect(current_status).to eq :lost
            end

            it "creates a new player board" do
              expect(model.board).not_to eq og_player_board.board
            end
          end
        end
      end

      context "when clicking the bomb spot" do
        describe "immedietly" do
          before do
            click_bomb
          end

          it "renders GAME OVER" do
            expect(page).to have_content("Game Over")
          end

          it "reveals the all of the spots on the board" do
            expect(board_revealed?).to eq true
          end

          it "renders the Elapsed Time" do
            expect_elapsed_time
          end
        end

        describe "after clicking a non bomb spot" do
          let!(:spot) { find(spot_selector, match: :first) }

          before do
            spot.click

            sleep(0.25)

            click_bomb
          end

          it "renders GAME OVER" do
            expect(page).to have_content("Game Over")
          end

          it "maintains board state" do
            expect(revealed?(spot.reload)).to eq true
          end

          it "renders the Elapsed Time" do
            expect_elapsed_time
          end
        end
      end

      context "when clicking all of the playable spots" do
        before do
          process_win_for(user)
        end

        it "renders the winning screen" do
          expect(page).to have_content("You Win!")

          top_board_players = all(".top-players").first

          expect(board_revealed?).to eq true
          expect(page).to have_content("Top Board Players")
          expect(top_board_players).to have_content(user.name)

          expect_elapsed_time
        end
      end
    end
  end

  def expect_elapsed_time
    expect(page).to have_content("Your Time: ")
    expect(page).to have_content(
      "Seconds: #{user.player_boards.last&.elapsed_time.to_i}",
    )
  end
end
