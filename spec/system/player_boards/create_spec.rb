require "rails_helper"
require "./spec/support/shared/board_examples.rb"
require "./spec/support/helpers/board_spec_helper.rb"
require "./spec/support/helpers/authentication_spec_helper.rb"

RSpec.describe "Create PlayedBoard", type: :feature, js: true do
  include SystemSpecHelper
  include BoardSpecHelper
  include AuthenticationSpecHelper

  context "when an authenticated user" do
    let!(:game) { create(:game) }
    let!(:user) { game.user }
    let!(:count) { PlayerBoard.count }
    let(:model) { user.player_boards.last }

    before do
      authenticate_user(user)
    end

    describe "starts a new game" do
      before do
        visit(game_path(game))
      end

      describe "the player board is ONLY created when a playable spot is clicked" do
        it "maintains count" do
          expect(PlayerBoard.count).to eq count
        end

        context "clicks a non bomb spot" do
          before do
            first_playable_spot.click
          end

          it "is commited" do
            expect(PlayerBoard.count).to eq count + 1
          end

          context "clicks another non bomb spot" do
            let!(:original_status) { model.status.try(:to_sym) }
            let!(:updated_at) { model.updated_at }

            before do
              first_playable_spot.click
            end

            it "has the 'playing' status" do
              expect(original_status).to eq :playing
            end

            it "updates the PlayerBoard" do
              expect(updated_at).not_to eq model.reload.updated_at
            end
          end
        end

        context "when winning a board game" do
          before do
            process_win_for(user)
          end

          it "renders the winning screen" do
            expect(page).to have_content("You Win!")
            # it "updates the status of the player board" do
            expect(model.reload.status.to_sym).to eq :won
          end
        end

        context "when immediately losing a board game" do
          before do
            click_bomb
          end

          it "renders GAME OVER" do
            expect(page).to have_content("Game Over")
          end

          it "does NOT create a new PlayerBoard instance" do
            expect(PlayerBoard.count).to eq count
          end
        end

        context "after clicking a playable spot" do
          before do
            first_playable_spot.click
          end

          describe "the play clicks a bomb and loses" do
            let!(:original_status) { model.status.try(:to_sym) }
            let!(:updated_at) { model.updated_at }

            before do
              click_bomb
            end

            it "renders GAME OVER" do
              expect(page).to have_content("Game Over")
            end

            it "creates a new PlayerBoard instance" do
              expect(PlayerBoard.count).to eq count + 1
            end

            it "updates the status of the player board" do
              actual = model.reload.status.try(:to_sym)

              expect(actual).not_to eq original_status
              expect(actual).to eq :lost
            end
          end
        end

        context "when flagging a spot" do
          before do
            first_bomb_spot.right_click
          end

          it "renders a flag" do
            expect(first_bomb_spot.text).to eq "🏁"
          end

          context "when right clicking the spot again" do
            before do
              first_bomb_spot.right_click
            end

            it "renders a blank spot" do
              expect(first_bomb_spot.text).to eq ""
            end
          end

          context "when WINNING" do
            before do
              click_all_playable_spots
            end

            it "renders the bomb" do
              expect(first_bomb_spot.text).to eq "💣"
            end
          end

          context "when GAME OVER from clicking a different bomb spot" do
            before do
              first_bomb_spot.click
            end

            it "renders the bomb" do
              expect(first_bomb_spot.text).to eq "💣"
            end
          end

          context "when GAME OVER from clicking a different bomb spot" do
            before do
              all_bomb_spots.last.click
            end

            it "renders the bomb" do
              expect(first_bomb_spot.text).to eq "💣"
            end
          end
        end
      end
    end
  end
end
