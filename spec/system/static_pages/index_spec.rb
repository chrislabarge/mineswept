require "rails_helper"

RSpec.describe "Landing Page", type: :feature do
  describe "visiting the landing page", js: true do
    before do
      visit("/")
    end

    it 'renders the welcome view' do
      expect(page).to have_content("Welcome to Mine Swept")
    end
  end
end
