require "rails_helper"

require "./spec/support/helpers/authentication_spec_helper.rb"
require "./spec/support/helpers/top_players_spec_helper.rb"

RSpec.describe "Top Players : Index", type: :feature do
  include SystemSpecHelper
  include AuthenticationSpecHelper
  include TopPlayersSpecHelper

  context "when an authenticated user" do
    let!(:user) { create(:user) }

    before do
      authenticate_user(user)
    end

    describe "visits the top players index" do
      context "when there are no top players" do
        before do
          visit("/top_players")
        end

        it "renders the view header" do
          expect(page).to have_content("Top Players")
        end

        it "does NOT render the top players" do
          expect(top_player_index).
            not_to have_content(user.name)
        end
      end

      context "when there are more than 20 top players" do
        before do
          create_list(:player_board, 21, status: :won)

          visit(top_players_path)
        end

        it "renders the pagination", js: true do
          click_on("2")

          expect(top_player_index).to have_content(User.last.name)
        end
      end

      context "when there are 20 or less top players" do
        before do
          create_list(:player_board, 19, status: :won)

          visit(top_players_path)
        end

        it "renders the pagination" do
          expect(page).not_to have_content("Next")
        end
      end
    end

    context "when there is a played board" do
      let!(:player_board) { create(:player_board, user: user) }

      context "with a win" do
        before do
          player_board.update(status: :won)

          visit("/top_players")
        end

        it "renders the top players" do
          expect(top_player_index).to have_content(user.name)
          expect(top_player_index).
            to have_content("#{user.win_count} Games Won")
          expect(top_player_index).
            to have_content("#{user.lost_count} Games Lost")
        end

        context "when there is another user with more wins" do
          let!(:another_user) { create(:user) }

          before do
            create_list(:player_board, 2, user: another_user, status: :won)

            visit("/top_players")
          end

          it "renders the user with more wins first" do
            first_player = find(".top-players").find("li", match: :first)

            expect(first_player).to have_content(another_user.name)
          end
        end
      end

      context "with a loss" do
        before do
          player_board.update(status: :lost)

          visit("/top_players")
        end

        it "renders the top players" do
          expect(top_player_index).not_to have_content(user.name)
        end
      end
    end
  end
end
