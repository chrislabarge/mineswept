require "rails_helper"
require "./spec/support/shared/board_examples.rb"
require "./spec/support/helpers/board_spec_helper.rb"
require "./spec/support/helpers/user_spec_helper.rb"
require "./spec/support/helpers/authentication_spec_helper.rb"
require "./spec/support/shared/advertised_examples.rb"

RSpec.describe "Show User", type: :feature do
  include SystemSpecHelper
  include BoardSpecHelper
  include UserSpecHelper
  include AuthenticationSpecHelper

  context "when an authenticated user" do
    let!(:user) { create(:user) }

    before do
      authenticate_user(user)
    end

    describe "navigates to their show" do
      before do
        visit user_path user
      end

      context "when the user has played a game" do
        it "renders the win/lose count" do
          allow_any_instance_of(User).to receive(:play_count).and_return(1)
          allow_any_instance_of(User).to receive(:win_count).and_return(1)

          visit user_path user

          expect(page).to have_content("1 Games Played")
          expect(page).to have_content("1 Games Won")
          expect(page).to have_content("0 Games Lost")
        end
      end

      it "renders the difficulty form" do
        expect(page).to have_content("Difficulty")
      end
    end
  end

  context "when a guest user" do
    before do
      visit("/")

      click_on("Play Game", match: :first)

      # Wait for page to render
      sleep(0.25)
    end

    describe "navigates to their show" do
      before do
        visit user_path(User.last)
      end

      it "does not render the difficulty form" do
        expect(page).not_to have_content("Difficulty")
      end

      it_behaves_like("A Page with Advertised Features")

      it "Asks the guest to Sign Up" do
        expect(page).to have_content(I18n.t(:sign_up))
      end
    end
  end
end
