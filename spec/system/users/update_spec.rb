require "rails_helper"
require "./spec/support/shared/board_examples.rb"
require "./spec/support/helpers/board_spec_helper.rb"
require "./spec/support/helpers/user_spec_helper.rb"
require "./spec/support/helpers/authentication_spec_helper.rb"

RSpec.describe "Update User", type: :feature, js: true do
  include SystemSpecHelper
  include BoardSpecHelper
  include UserSpecHelper
  include AuthenticationSpecHelper
end
