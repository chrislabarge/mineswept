require "rails_helper"

Rails.application.load_tasks

describe "boards.rake" do
  let!(:resource) { create(:board) }
  let!(:count) { Board.count }

  describe "clear boards" do
    before do
      Rake::Task["boards:clear"].invoke
    end

    it "destroys the boards" do
      expect(Board.count).to eq count - 1
    end
  end
end
